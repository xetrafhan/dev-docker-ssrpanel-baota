### Build [SSRPanel](https://github.com/ssrpanel/SSRPanel) with [Baota](http://www.bt.cn/)

Use Docker for building SSRPanel website driven by Baota

## Run
```bash
docker build -t docker-ssr-panel-baota .
docker run -d -P docker-ssr-panel-baota 
```